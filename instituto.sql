﻿-- Creacion de base de datos correspondiente al ejercicio 3 de la hoja 1
-- Tiene 6 tablas

DROP DATABASE IF EXISTS b20190531;
CREATE DATABASE b20190531;
USE b20190531;

/*
  Creacion de la tabla profesores
*/
CREATE TABLE profesores(
  dni varchar (9),
  nombre varchar (100),
  direccion varchar (100),
  telefono varchar (12),
  PRIMARY KEY (dni)
);

/*
  Creacion de la tabla modulos
*/
CREATE TABLE modulos(
  codigo int AUTO_INCREMENT,
  nombre varchar (100),
  PRIMARY KEY (codigo)
);

/*
  Creacion de la tabla alumnos
*/
CREATE TABLE alumnos(
  expediente int AUTO_INCREMENT,
  nombre varchar (100),
  apellidos varchar (100),
  fecha_nac date,
  PRIMARY KEY (expediente)
);

/*
  Creacion de la tabla imparte
*/
CREATE TABLE imparte(
  dniProfesor varchar (9),
  codigoModulo int,
  PRIMARY KEY (dniProfesor, codigoModulo),
  UNIQUE KEY (codigoModulo),
  CONSTRAINT fkImpartenProfesores FOREIGN KEY (dniProfesor) REFERENCES profesores(dni),
  CONSTRAINT fkImpartenModulos FOREIGN KEY (codigoModulo) REFERENCES modulos(codigo)
);

/*
  Creacion de la tabla cursa
*/
CREATE TABLE cursa(
  codModulo int,
  expedienteAlumno int,
  PRIMARY KEY (codModulo, expedienteAlumno),
  CONSTRAINT fkCursaModulos FOREIGN KEY (codModulo) REFERENCES modulos(codigo),
  CONSTRAINT fkCursaAlumnos FOREIGN KEY (expedienteAlumno) REFERENCES alumnos(expediente)
);

/*
  Creacion de la tabla delegar
*/
CREATE TABLE delegar(
  expedienteDelegado int,
  expedienteEstudiante int,
  PRIMARY KEY (expedienteDelegado, expedienteEstudiante),
  UNIQUE KEY (expedienteEstudiante),
  CONSTRAINT fkDelegarDelegado FOREIGN KEY (expedienteDelegado) REFERENCES alumnos(expediente),
  CONSTRAINT fkDelegarAlumnos FOREIGN KEY (expedienteEstudiante) REFERENCES alumnos(expediente)
);

-- insercion de datos

-- insertar en tabla profesores
  INSERT INTO profesores (dni, nombre, direccion, telefono) VALUES 
  ('45874698D', 'Ramon', 'Pasaje de Peña', '678954123'),
  ('21368549N' ,'Daniel', 'Calle Alta', '621487638');

-- insertar en tabla modulos
  INSERT INTO modulos (nombre) VALUES 
  ('Sistemas de Información'),
  ('Bases de Datos');

-- insertar en tabla alumnos
  INSERT INTO alumnos (nombre, apellidos, fecha_nac) VALUES 
  ('Alejandro', 'Ortiz', '1990/9/5'),
  ('Luis', 'López', '1986/4/23'),
  ('Pablo', 'Herrero', '1988/12/15');

-- insertar en tabla imparte
  INSERT INTO imparte (dniProfesor, codigoModulo) VALUES 
  ('45874698D', 2),
  ('21368549N', 1);

-- insertar en tabla cursa
  INSERT INTO cursa (codModulo, expedienteAlumno) VALUES 
  (1, 1),
  (1, 2),
  (1, 3),
  (2, 1),
  (2, 3);

-- insertar en tabla delegar
  INSERT INTO delegar (expedienteDelegado, expedienteEstudiante) VALUES 
  (2, 1),
  (2, 3);